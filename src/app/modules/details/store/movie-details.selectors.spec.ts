import {MovieDetailsState} from '@app/modules/details/store/movie-details.reducer';
import {loadedMovieId, selectIsLoading, selectMovie} from './movie-details.selectors';

describe('Store: Movie Details Selectors', () => {
  let state: MovieDetailsState;
  beforeEach(() => {
    state = {
      movie: {},
      isLoading: true,
      loadedMovieId: 10,
    };
  });

  it('should return loaded movie id', () => {
    expect(loadedMovieId.projector(state)).toBe(state.loadedMovieId);
  });

  it('should return movie', () => {
    expect(selectMovie.projector(state)).toBe(state.movie);
  });

  it('should return isLoading', () => {
    expect(selectIsLoading.projector(state)).toBe(true);
  });
});
