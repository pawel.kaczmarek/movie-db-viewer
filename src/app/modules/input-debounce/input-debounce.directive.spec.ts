import {Component} from '@angular/core';
import {fakeAsync, tick} from '@angular/core/testing';
import {environment} from '@env/environment';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MdbvInputDebounceDirective} from './input-debounce.directive';

@Component({
  selector: 'mdbv-custom-host',
  template: `<input (mdbvInputDebounce)="readQuery($event)"/>`
})
class CustomHostComponent {
  readQuery(query: string) {
  }
}

describe('Directive: MdbvTriggerSearchDirective', () => {
  let spectator: Spectator<CustomHostComponent>;

  const getInput = () => spectator.query('input');
  const getDirective = () => spectator.query(MdbvInputDebounceDirective);

  const createComponent = createComponentFactory({
    component: CustomHostComponent,
    declarations: [MdbvInputDebounceDirective]
  });

  beforeEach(() => {
    environment.debounce = 100;
    spectator = createComponent();
    spyOn(spectator.component, 'readQuery');
  });

  it('should debounce when user types in', fakeAsync(() => {
    spectator.typeInElement('first value', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(80);
    spectator.typeInElement('second value', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(80);
    spectator.typeInElement('third value', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(80);
    spectator.typeInElement('snafu', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(200);
    expect(spectator.component.readQuery).toHaveBeenCalledTimes(1);
    expect(spectator.component.readQuery).toHaveBeenCalledWith('snafu');
  }));

  it('should not emit empty values when allowFalse = false', fakeAsync(() => {
    spectator.typeInElement('snafu', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(120);
    spectator.typeInElement('', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(120);
    expect(spectator.component.readQuery).toHaveBeenCalledTimes(1);
    expect(spectator.component.readQuery).toHaveBeenCalledWith('snafu');
  }));

  it('should not emit empty values when allowFalse = true', fakeAsync(() => {
    getDirective().allowEmpty = true;
    spectator.detectChanges();
    spectator.typeInElement('snafu', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(120);
    spectator.typeInElement('', getInput());
    spectator.dispatchKeyboardEvent('input', 'keyup', 'W', getInput());
    tick(120);
    expect(spectator.component.readQuery).toHaveBeenCalledTimes(2);
    expect(spectator.component.readQuery).toHaveBeenCalledWith('snafu');
    expect(spectator.component.readQuery).toHaveBeenCalledWith('');
  }));
});
