import {SearchState} from '@app/modules/search/store/movie-search.reducer';
import {
  selectLastPicks,
  selectSearchResults,
  selectTopSearchResults,
  selectUserPicks,
} from '@app/modules/search/store/movie-search.selectors';

describe('Store: Movie Search Selectors', () => {
  let state: SearchState;
  const movies = [{id: 1}, {id: 2}, {id: 3}];
  const picks = [{id: 40}, {id: 40}, {id: 40}, {id: 1}, {id: 1}, {id: 50}];
  beforeEach(() => {
    state = {
      entities: {1: movies[0], 2: movies[1], 3: movies[2]},
      ids: [1, 2, 3],
      isLoading: false,
      userPicks: picks
    };
  });

  it('should return search results', () => {
    expect(selectSearchResults.projector(state)).toEqual(movies);
  });

  it('should return user picks', () => {
    expect(selectUserPicks.projector(state)).toEqual(picks);
  });

  it('should return search results', () => {
    expect(selectTopSearchResults.projector(movies, {limit: 2})).toEqual([{id: 1}, {id: 2}]);
  });

  it('should return user last picks', () => {
    expect(selectLastPicks.projector(picks, {limit: 2})).toEqual([{id: 40}, {id: 1}]);
    expect(selectLastPicks.projector(picks, {limit: 3})).toEqual([{id: 40}, {id: 1}, {id: 50}]);
  });
});
