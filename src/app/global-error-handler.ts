import {ErrorHandler, Injectable} from '@angular/core';
import {ErrorsService} from '@app/errors.service';
import {environment} from '@env/environment';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private errorService: ErrorsService) {
  }

  handleError(error) {
    if (environment.production) {
      this.errorService.log(error);
    }
    throw error;
  }
}
