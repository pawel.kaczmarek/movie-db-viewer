import {AppState} from '@app/store/app.store';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {adapter, BrowserState, browserStateKey} from './browser-movies.reducer';

export const selectBrowserState = createFeatureSelector<AppState, BrowserState>(browserStateKey);

const {
  selectAll,
} = adapter.getSelectors();

export const selectSelectedId = createSelector(selectBrowserState, state => state.selectedItemId);
export const selectCurrentPage = createSelector(selectBrowserState, state => state.currentPage);
export const selectIsLastPage = createSelector(selectBrowserState, state => state.isLastPage);
export const selectIsLoading = createSelector(selectBrowserState, state => state.isLoading);
export const selectAllMovies = createSelector(selectBrowserState, selectAll);
