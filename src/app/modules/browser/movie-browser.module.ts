import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MdbvLoadMoreButtonComponent} from '@app/modules/browser/list/load-more-button/load-more-button.component';
import {MdbvIllustrationsModule} from '@app/modules/illustrations/illustrations.module';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {MdbvMovieListItemComponent} from './list/item/movie-list-item.component';
import {MdbvMovieListComponent} from './list/movie-list.component';
import {browserReducer, browserStateKey} from './store';
import {BrowserMoviesEffects} from './store/browser-movies.effects';

@NgModule({
  declarations: [
    MdbvMovieListComponent,
    MdbvMovieListItemComponent,
    MdbvLoadMoreButtonComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    EffectsModule.forFeature([BrowserMoviesEffects]),
    StoreModule.forFeature(browserStateKey, browserReducer),
    MdbvIllustrationsModule,
  ],
  exports: [
    MdbvMovieListComponent,
  ]
})
export class MdbvMovieBrowserModule { }
