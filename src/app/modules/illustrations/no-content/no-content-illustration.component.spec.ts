import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {testSubjSelector} from '@testing';
import {MdbvNoContentIllustrationComponent} from './no-content-illustration.component';

describe('Component: MdbvNoContentIllustrationComponent', () => {
  let spectator: Spectator<MdbvNoContentIllustrationComponent>;
  let testConfig;

  const getTitle = () => spectator.query(testSubjSelector('mdbv-no-content-illustration-title'));
  const getSubtitle = () => spectator.query(testSubjSelector('mdbv-no-content-illustration-subtitle'));

  const createComponent = createComponentFactory(MdbvNoContentIllustrationComponent);

  beforeEach(() => {
    testConfig = {
      title: 'random title',
      subtitle: 'random subtitle',
    };
    spectator = createComponent({
      props: {
        title: testConfig.title,
        subtitle: testConfig.subtitle
      }
    });
    spectator.detectChanges();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });

  it('should display title', () => {
    expect(getTitle()).toContainText(testConfig.title);
  });

  it('should display subtitle', () => {
    expect(getSubtitle()).toContainText(testConfig.subtitle);
  });
});
