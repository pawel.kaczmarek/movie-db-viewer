import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AttachApiKeyInterceptor implements HttpInterceptor {

  // tslint:disable-next-line:no-any
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.startsWith(environment.movieDb.apiUrl)) {
      return next.handle(request.clone({
        params: request.params.append('api_key', environment.movieDb.apiKey)
      }));
    } else {
      return next.handle(request);
    }
  }
}
