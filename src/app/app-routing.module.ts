import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MdbvMovieListComponent} from '@app/modules/browser/list/movie-list.component';
import {MdbvMovieDetailsComponent} from '@app/modules/details/movie-details.component';

const routes: Routes = [
  {
    path: '',
    component: MdbvMovieListComponent,
    outlet: 'browser'
  },
  {
    path: '',
    component: MdbvMovieDetailsComponent,
    outlet: 'details'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
