import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'mdbv-no-content-illustration',
  templateUrl: './no-content-illustration.component.html',
  styleUrls: ['./no-content-illustration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdbvNoContentIllustrationComponent {

  @Input() title: string;
  @Input() subtitle: string;

}
