import {NgModule} from '@angular/core';
import {MdbvLoadingIconComponent} from './loading-icon/loading-icon.component';
import {MdbvNoContentIllustrationComponent} from './no-content/no-content-illustration.component';

@NgModule({
  declarations: [
    MdbvLoadingIconComponent,
    MdbvNoContentIllustrationComponent,
  ],
  exports: [
    MdbvLoadingIconComponent,
    MdbvNoContentIllustrationComponent,
  ]
})
export class MdbvIllustrationsModule { }
