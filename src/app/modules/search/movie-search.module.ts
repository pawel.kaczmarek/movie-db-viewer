import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MdbvInputDebounceModule} from '@app/modules/input-debounce/input-debounce.module';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {MdbvIllustrationsModule} from '../illustrations/illustrations.module';
import {MdbvMovieSearchBarComponent} from './search-bar/movie-search-bar.component';
import {searchReducer, searchStateKey} from './store';
import {MovieSearchEffects} from './store/movie-search.effects';

@NgModule({
  declarations: [
    MdbvMovieSearchBarComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MdbvIllustrationsModule,
    MdbvInputDebounceModule,
    RouterModule,
    EffectsModule.forFeature([MovieSearchEffects]),
    StoreModule.forFeature(searchStateKey, searchReducer),
  ],
  exports: [
    MdbvMovieSearchBarComponent,
  ]
})
export class MdbvMovieSearchModule { }
