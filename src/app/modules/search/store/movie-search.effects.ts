import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {MovieSearchService} from '../movie-search.service';
import {searchMovie, searchMovieComplete, searchMovieFailed} from './movie-search.actions';

@Injectable()
export class MovieSearchEffects {

  constructor(
    private store: Store,
    private actions$: Actions,
    private searchService: MovieSearchService
  ) {}

  searchMovies$ = createEffect(() => this.actions$.pipe(
    ofType(searchMovie),
    switchMap((action) => this.searchService.search(action.query).pipe(
      map(page => searchMovieComplete({page}),
      catchError( error => of(searchMovieFailed({error})))
    )))
  ));
}
