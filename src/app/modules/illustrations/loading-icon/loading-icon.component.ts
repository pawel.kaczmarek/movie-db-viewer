import {Component} from '@angular/core';

@Component({
  selector: 'mdbv-loading-icon',
  template: `
    <span class="slds-icon-typing slds-is-animated" title="User is typing">
      <span class="slds-icon-typing__dot"></span>
      <span class="slds-icon-typing__dot"></span>
      <span class="slds-icon-typing__dot"></span>
      <span class="slds-assistive-text">Loading</span>
    </span>`
})
export class MdbvLoadingIconComponent {
}
