import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {MdbvMoviePosterComponent} from '@app/modules/details/poster/movie-poster.component';
import {detailsStateKey, movieDetailsReducer} from '@app/modules/details/store';
import {MovieDetailsEffects} from '@app/modules/details/store/movie-details.effects';
import {MdbvIllustrationsModule} from '@app/modules/illustrations/illustrations.module';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {NglRatingsModule} from 'ng-lightning';
import {MdbvMovieDetailsComponent} from './movie-details.component';

@NgModule({
  declarations: [
    MdbvMovieDetailsComponent,
    MdbvMoviePosterComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MdbvIllustrationsModule,
    NglRatingsModule,
    EffectsModule.forFeature([MovieDetailsEffects]),
    StoreModule.forFeature(detailsStateKey, movieDetailsReducer),
  ],
  exports: [
    MdbvMovieDetailsComponent,
  ]
})
export class MdbvMovieDetailsModule { }
