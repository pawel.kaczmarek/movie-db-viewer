import {BrowserState} from '@app/modules/browser/store';
import {MovieDetailsState} from '@app/modules/details/store';
import {SearchState} from '@app/modules/search/store';

export interface AppState {
  browser?: BrowserState;
  details?: MovieDetailsState;
  search?: SearchState;
}
