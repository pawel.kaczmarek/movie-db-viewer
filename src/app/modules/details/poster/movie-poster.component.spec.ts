import {MdbvMoviePosterComponent} from '@app/modules/details/poster/movie-poster.component';
import {MdbvLoadingIconComponent} from '@app/modules/illustrations/loading-icon/loading-icon.component';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {testSubjSelector} from '@testing';
import {MockComponent} from 'ng-mocks';

describe('Component: MdbvMoviePosterComponent', () => {
  let spectator: Spectator<MdbvMoviePosterComponent>;

  const getImg = () => spectator.query(testSubjSelector('mdbv-movie-poster-img')) as HTMLImageElement;
  const getLoadingPlaceholder = () => spectator.query(MdbvLoadingIconComponent);

  const createComponent = createComponentFactory({
    component: MdbvMoviePosterComponent,
    declarations: [
      MockComponent(MdbvLoadingIconComponent),
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });

  it('should hide poster image by default', () => {
    expect(getImg()).toBeHidden();
  });

  it('should show loading placeholder', () => {
    expect(getLoadingPlaceholder()).toExist();
  });

  describe('when image is loaded', () => {
    beforeEach(() => {
      getImg().dispatchEvent(new Event('load'));
      spectator.detectChanges();
    });
    it('should show image', () => {
      expect(getImg()).not.toBeHidden();
    });
    it('should hide image placeholder', () => {
      expect(getLoadingPlaceholder()).not.toExist();
    });

    describe('when image url is set again', () => {
      beforeEach(() => {
        spectator.setInput({url: 'random'});
        spectator.detectChanges();
      });
      it('should hide image', () => {
        expect(getImg()).toBeHidden();
      });
      it('should loading placeholder', () => {
        expect(getLoadingPlaceholder()).toExist();
      });
    });
  });

  describe('when poster url is null', () => {
    beforeEach(() => {
      spectator.setInput({url: null});
      spectator.detectChanges();
    });
    it('should show image with static no-content image', () => {
      expect(getImg().src).toContain('/assets/no-movie-icon.svg');
    });
  });
});
