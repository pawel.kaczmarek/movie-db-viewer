import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'mdbv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'movie-db-viewer';
}
