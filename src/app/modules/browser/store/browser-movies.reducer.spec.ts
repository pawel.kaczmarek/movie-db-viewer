import {Movie, Pageable} from '@app/app.models';
import {clearMovies, deselectMovie, loadMovies, loadMoviesComplete, loadMoviesFailed, selectMovie} from './browser-movies.actions';
import {browserReducer, BrowserState} from './browser-movies.reducer';

describe('Store: Movie Browser Reducer', () => {
  let state: BrowserState;
  let afterState: BrowserState;
  const previousMovies = [{id: 1}, {id: 2}, {id: 3}];
  beforeEach(() => {
    state = {
      entities: {1: previousMovies[0], 2: previousMovies[1], 3: previousMovies[2]},
      ids: [1, 2, 3],
      currentPage: 3,
      isLastPage: true,
      selectedItemId: 10,
      isLoading: true,
    };
  });

  it('should handle selectMovie', () => {
    afterState = browserReducer(state, selectMovie({id: 1}));
    expect(afterState).toEqual({
      ...state,
      selectedItemId: 1
    });
  });

  it('should handle deselectMovie', () => {
    afterState = browserReducer(state, deselectMovie());
    expect(afterState).toEqual({
      ...state,
      selectedItemId: null
    });
  });

  it('should handle clearMovies', () => {
    afterState = browserReducer(state, clearMovies());
    expect(afterState).toEqual({
      ...state,
      entities: {},
      ids: [],
      isLastPage: true,
      currentPage: 0
    });
  });

  it('should handle loadMovies', () => {
    afterState = browserReducer(state, loadMovies());
    expect(afterState).toEqual({
      ...state,
      isLoading: true
    });
  });

  it('should handle loadMoviesFailed', () => {
    afterState = browserReducer(state, loadMoviesFailed({error: {}}));
    expect(afterState).toEqual({
      ...state,
      isLoading: false
    });
  });

  it('should handle loadMoviesComplete', () => {
    afterState = browserReducer(state, loadMoviesComplete({
      page: {
        results: [{id: 10}, {id: 20}],
        page: 6,
        total_pages: 7
      } as Pageable<Movie>
    }));
    expect(afterState).toEqual({
      ...state,
      isLoading: false,
      currentPage: 6,
      isLastPage: false,
      entities: {
        ...state.entities,
        10: {id: 10},
        20: {id: 20}
      },
      ids: [1, 2, 3, 10, 20]
    });
  });
});
