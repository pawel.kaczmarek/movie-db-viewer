export const environment = {
  production: true,
  debounce: 500,
  movieDb: {
    apiUrl: 'https://api.themoviedb.org/3',
    apiKey: `4ff9d08260ed338797caa272d7df35dd`,
    imageUrl: 'https://image.tmdb.org/t/p/w185',
  },
};
