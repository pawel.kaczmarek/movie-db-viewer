import {Movie, Pageable} from '@app/app.models';
import {MovieBrowserService} from '@app/modules/browser/movie-browser.service';
import {environment} from '@env/environment';
import {createHttpFactory, HttpMethod, SpectatorHttp} from '@ngneat/spectator';

describe('Service: MovieBrowserService', () => {
  let spectator: SpectatorHttp<MovieBrowserService>;
  const createHttp = createHttpFactory(MovieBrowserService);
  const API_URL = 'some.url';

  beforeEach(() => {
    spectator = createHttp();
    environment.movieDb.apiUrl = API_URL;
  });

  it('should fetch popular movies', () => {
    const response = {} as Pageable<Movie>;
    spectator.service.getPopular(3).subscribe(res => {
      expect(res).toEqual(response);
    });
    const req = spectator.expectOne(`${API_URL}/discover/movie?sort_by=popularity.desc&page=3`, HttpMethod.GET);
    req.flush(response);
  });
});
