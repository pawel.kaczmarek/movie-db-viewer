import {Movie} from '@app/app.models';
import {AppState} from '@app/store/app.store';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {adapter, SearchState, searchStateKey} from './movie-search.reducer';

export const selectMovieSearch = createFeatureSelector<AppState, SearchState>(searchStateKey);

const {
  selectAll,
} = adapter.getSelectors();

const unique = (movies: Movie[]): Movie[] => [...new Set(movies.map(item => item.id))]
  .map(id => movies.find(movie => movie.id === id));

export const selectSearchIsLoading = createSelector(selectMovieSearch, state => state.isLoading);
export const selectUserPicks = createSelector(selectMovieSearch, state => state.userPicks);
export const selectSearchResults = createSelector(selectMovieSearch, selectAll);

export const selectTopSearchResults = createSelector(
  selectSearchResults,
  (results, props) => results.slice(0, props.limit)
);

export const selectLastPicks = createSelector(
  selectUserPicks,
  (picks, props) => unique(picks).slice(0, props.limit)
);
