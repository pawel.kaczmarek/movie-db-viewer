import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MdbvInputDebounceDirective} from './input-debounce.directive';

@NgModule({
  declarations: [
    MdbvInputDebounceDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    MdbvInputDebounceDirective,
  ]
})
export class MdbvInputDebounceModule {
}
