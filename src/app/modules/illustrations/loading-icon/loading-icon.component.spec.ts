import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MdbvLoadingIconComponent} from './loading-icon.component';

describe('Component: MdbvLoadingIconComponent', () => {
  let spectator: Spectator<MdbvLoadingIconComponent>;

  const createComponent = createComponentFactory(MdbvLoadingIconComponent);

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });
});
