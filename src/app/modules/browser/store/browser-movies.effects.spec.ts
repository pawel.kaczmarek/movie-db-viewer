import {selectCurrentPage} from '@app/modules/browser/store/browser-movies.selectors';
import {createServiceFactory, SpectatorService} from '@ngneat/spectator';
import {provideMockActions} from '@ngrx/effects/testing';
import {Action} from '@ngrx/store';
import SpyObj = jasmine.SpyObj;
import {provideMockStore} from '@ngrx/store/testing';
import {Observable, of} from 'rxjs';
import {MovieBrowserService} from '../movie-browser.service';
import {loadMovies} from './browser-movies.actions';
import {BrowserMoviesEffects} from './browser-movies.effects';

xdescribe('Store: Movie Browser Effects', () => {
  let spectator: SpectatorService<BrowserMoviesEffects>;
  let browserService: SpyObj<MovieBrowserService>;
  let actions$ = new Observable<Action>();
  const currentPage = 10;

  const createService = createServiceFactory({
    service: BrowserMoviesEffects,
    mocks: [
      MovieBrowserService,
    ],
    providers: [
      provideMockActions(() => actions$),
      provideMockStore({
        selectors: [
          {
            selector: selectCurrentPage,
            value: currentPage
          },
        ]
      }),
    ]
  });

  beforeEach(() => {
    spectator = createService();
    browserService = spectator.inject(MovieBrowserService);
  });

  it('should create', () => {
    expect(spectator.service).toBeDefined();
  });

  describe('load movies effect', () => {
    beforeEach(() => {
      actions$ = of(loadMovies());
    });
    it('should load next page of movies', () => {
      spectator.service.loadMovies$.subscribe();
      expect(browserService.getPopular).toHaveBeenCalledWith(currentPage + 1);
    });
  });
});
