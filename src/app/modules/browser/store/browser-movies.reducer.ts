import {Movie} from '@app/app.models';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import {clearMovies, deselectMovie, loadMovies, loadMoviesComplete, loadMoviesFailed, selectMovie} from './browser-movies.actions';

export const browserStateKey = 'browser';

export interface BrowserState extends EntityState<Movie> {
  currentPage: number;
  isLastPage: boolean;
  isLoading: boolean;
  selectedItemId: number | null;
}

export const initialState: BrowserState = {
  currentPage: 0,
  isLastPage: true,
  isLoading: false,
  entities: {},
  ids: [],
  selectedItemId: null,
};

export const browserReducer = createReducer(
  initialState,
  on(selectMovie, (state, {id}) => ({...state, selectedItemId: id})),
  on(deselectMovie, state => ({...state, selectedItemId: null})),
  on(clearMovies, state => adapter.removeAll({
    ...state,
    currentPage: initialState.currentPage,
    isLastPage: initialState.isLastPage,
  })),
  on(loadMovies, state => ({...state, isLoading: true})),
  on(loadMoviesComplete, loadMoviesFailed, state => ({...state, isLoading: false})),
  on(loadMoviesComplete, (state, {page}) => adapter.upsertMany(page.results, {
    ...state,
    currentPage: page.page,
    isLastPage: page.page === page.total_pages,
  })),
);

// ToDo: get back here and check if build work without it
// export function reducer(state, action) {
//   return browseMoviesReducer(state, action);
// }

export const adapter: EntityAdapter<Movie> = createEntityAdapter<Movie>();
