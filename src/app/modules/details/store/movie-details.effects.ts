import {Injectable} from '@angular/core';
import {loadedMovieId} from '@app/modules/details/store/movie-details.selectors';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {EMPTY, of} from 'rxjs';
import {catchError, concatMap, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {MovieDetailsService} from '../movie-details.service';
import {loadDetails, loadDetailsComplete, loadDetailsFailed} from './movie-details.actions';

@Injectable()
export class MovieDetailsEffects {

  constructor(
    private actions$: Actions,
    private detailsService: MovieDetailsService,
    private store: Store,
  ) {}

  loadDetails$ = createEffect(() => this.actions$.pipe(
    ofType(loadDetails),
    concatMap(action => of(action).pipe(
      withLatestFrom(this.store.pipe(select(loadedMovieId)))
    )),
    switchMap(([action, loadedId]) => action.movie.id !== loadedId
      ? this.detailsService.getDetails(action.movie.id).pipe(
          map(detailedMovie => loadDetailsComplete({movie: detailedMovie}),
          catchError( error => of(loadDetailsFailed({error})))))
      : EMPTY
    )
  ));
}
