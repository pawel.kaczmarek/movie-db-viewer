import {Movie} from '@app/app.models';
import {MdbvMovieListComponent} from '@app/modules/browser/list/movie-list.component';
import {clearMovies, loadMovies, selectAllMovies, selectIsLastPage, selectIsLoading, selectSelectedId} from '@app/modules/browser/store';
import {AppState} from '@app/store/app.store';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MemoizedSelector} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {testSubjSelector} from '@testing';
import {MockComponent} from 'ng-mocks';
import {MdbvMovieListItemComponent} from './item/movie-list-item.component';
import {MdbvLoadMoreButtonComponent} from './load-more-button/load-more-button.component';

describe('Component: MdbvMovieListComponent', () => {
  let spectator: Spectator<MdbvMovieListComponent>;
  let store: MockStore;
  let selectedIdSelector: MemoizedSelector<AppState, number>;
  let allMoviesSelector: MemoizedSelector<AppState, Movie[]>;
  let isLastPageSelector: MemoizedSelector<AppState, boolean>;

  const getNoItemsPlaceholder = () => spectator.query(testSubjSelector('mdbv-movie-list-no-items-placeholder'));
  const getItemsList = () => spectator.query(testSubjSelector('mdbv-movie-list-items-list'));
  const getAllItemComponents = () => spectator.queryAll(MdbvMovieListItemComponent);
  const getLoadMoreButton = () => spectator.queryAll(MdbvLoadMoreButtonComponent);
  const getAllListItems = () => spectator.queryAll(testSubjSelector('mdbv-movie-list-item'));

  const createComponent = createComponentFactory({
    component: MdbvMovieListComponent,
    declarations: [
      MockComponent(MdbvMovieListItemComponent),
      MockComponent(MdbvLoadMoreButtonComponent),
    ],
    detectChanges: false,
    providers: [
      provideMockStore({}),
    ]
  });

  beforeEach(() => {
    spectator = createComponent();
    store = spectator.inject(MockStore);
    spyOn(store, 'dispatch');
    selectedIdSelector = store.overrideSelector(selectSelectedId, null);
    allMoviesSelector = store.overrideSelector(selectAllMovies, []);
    isLastPageSelector = store.overrideSelector(selectIsLastPage, false);
    spectator.detectChanges();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });

  it('should dispatch clear movies action', () => {
    expect(store.dispatch).toHaveBeenCalledWith(clearMovies());
  });

  it('should dispatch fetch movies action', () => {
    expect(store.dispatch).toHaveBeenCalledWith(loadMovies());
  });

  it('should not render load more button when last page has been fetched', () => {
    isLastPageSelector.setResult(true);
    store.refreshState();
    spectator.detectChanges();
    expect(getLoadMoreButton()).not.toExist();
  });

  describe('when no movies are fetch', () => {
    beforeEach(() => {
      allMoviesSelector.setResult([]);
      store.refreshState();
      spectator.detectChanges();
    });
    it('should display no items placeholder', () => {
      expect(getNoItemsPlaceholder()).toExist();
    });
    it('should not display movies list', () => {
      expect(getItemsList()).not.toExist();
    });
  });

  describe('when there are fetched movies', () => {
    const movies = [{id: 1}, {id: 2}, {id: 3}];
    beforeEach(() => {
      allMoviesSelector.setResult(movies);
      store.refreshState();
      spectator.detectChanges();
    });
    it('should render list item for each movie', () => {
      expect(getAllItemComponents().length).toEqual(3);
    });
    it('should render load more button', () => {
      expect(getLoadMoreButton()).toExist();
    });
    it('should pass movie to each item', () => {
      expect(getAllItemComponents()[0].movie).toBe(movies[0]);
      expect(getAllItemComponents()[1].movie).toBe(movies[1]);
      expect(getAllItemComponents()[2].movie).toBe(movies[2]);
    });
    it('should mark selected one', () => {
      selectedIdSelector.setResult(2);
      store.refreshState();
      spectator.detectChanges();
      expect(getAllListItems()[0]).not.toHaveClass('slds-is-active');
      expect(getAllListItems()[1]).toHaveClass('slds-is-active');
      expect(getAllListItems()[2]).not.toHaveClass('slds-is-active');
    });
  });
});
