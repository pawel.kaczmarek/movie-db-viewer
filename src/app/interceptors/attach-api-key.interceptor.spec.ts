/* tslint:disable:no-any */
import {HttpParams, HttpRequest, HttpResponse} from '@angular/common/http';
import {environment} from '@env/environment';
import {createServiceFactory, SpectatorService} from '@ngneat/spectator';
import {of} from 'rxjs';
import {AttachApiKeyInterceptor} from './attach-api-key.interceptor';

describe('Interceptor: AttachApiKeyInterceptor', () => {
  let spectator: SpectatorService<AttachApiKeyInterceptor>;
  let apiDummyRequest: HttpRequest<any>;

  const createService = createServiceFactory(AttachApiKeyInterceptor);

  beforeEach(() => {
    spectator = createService();
  });

  it('should exist', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('should not attach api_key other calls', done => {
    apiDummyRequest = new HttpRequest('GET', `some.random.url/something`);
    const next = {
      handle(request: HttpRequest<any>) {
        expect(request.params.has('api_key')).toBeFalse();
        done();

        return of(new HttpResponse({status: 200}));
      },
    };
    spectator.service.intercept(apiDummyRequest, next).subscribe();
  });

  describe('when calling movie DB API', () => {
    beforeEach(() => {
      const params = new HttpParams().append('paramName', 'paramValue');
      apiDummyRequest = new HttpRequest('GET', `${environment.movieDb.apiUrl}/something`, {
        params,
      });
    });

    it('should preserve any other params already added to the request', done => {
      const next = {
        handle(request: HttpRequest<any>) {
          expect(request.params.has('paramName')).toBeTrue();
          expect(request.params.get('paramName')).toEqual('paramValue');
          done();
          return of(new HttpResponse({status: 200}));
        },
      };
      spectator.service.intercept(apiDummyRequest, next).subscribe();
    });

    it('should attach api_key to movie DP API calls', done => {
      const next = {
        handle(request: HttpRequest<any>) {
          expect(request.params.has('api_key')).toBeTrue();
          expect(request.params.get('api_key')).toEqual(environment.movieDb.apiKey);
          done();

          return of(new HttpResponse({status: 200}));
        },
      };
      spectator.service.intercept(apiDummyRequest, next).subscribe();
    });
  });
});
