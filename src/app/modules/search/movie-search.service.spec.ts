import {Movie, Pageable} from '@app/app.models';
import {MovieSearchService} from '@app/modules/search/movie-search.service';
import {environment} from '@env/environment';
import {createHttpFactory, HttpMethod, SpectatorHttp} from '@ngneat/spectator';

describe('Service: MovieSearchService', () => {
  let spectator: SpectatorHttp<MovieSearchService>;
  const createHttp = createHttpFactory(MovieSearchService);
  const API_URL = 'some.url';

  beforeEach(() => {
    spectator = createHttp();
    environment.movieDb.apiUrl = API_URL;
  });

  it('should call api with query', () => {
    const query = 'iddqd';
    const response = {} as Pageable<Movie>;
    spectator.service.search(query).subscribe(res => {
      expect(res).toEqual(response);
    });
    const req = spectator.expectOne(`${API_URL}/search/movie?query=iddqd`, HttpMethod.GET);
    req.flush(response);
  });
});
