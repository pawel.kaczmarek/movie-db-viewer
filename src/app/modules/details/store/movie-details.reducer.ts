import {createReducer, on} from '@ngrx/store';
import {MovieDetails} from '../../../app.models';
import {loadDetails, loadDetailsComplete, loadDetailsFailed} from './movie-details.actions';

export const detailsStateKey = 'details';

export interface MovieDetailsState {
  isLoading: boolean;
  loadedMovieId: number | null;
  movie: MovieDetails | null;
}

export const initialState: MovieDetailsState = {
  isLoading: false,
  loadedMovieId: null,
  movie: null,
};

export const movieDetailsReducer = createReducer(
  initialState,
  on(loadDetails, (state, {movie}) => ({
    ...state,
    movie: {...movie},
    isLoading: true
  })),
  on(loadDetailsComplete, loadDetailsFailed, state => ({...state, isLoading: false})),
  on(loadDetailsComplete, (state, {movie}) => ({
    ...state,
    movie,
    loadedMovieId: movie.id
  })),
);
