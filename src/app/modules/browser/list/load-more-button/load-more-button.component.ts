import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {shareReplay} from 'rxjs/internal/operators';
import {loadMovies, selectIsLoading} from '../../store';

@Component({
  selector: 'mdbv-load-more-button',
  templateUrl: './load-more-button.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdbvLoadMoreButtonComponent implements OnInit {

  isLoading$: Observable<boolean>;

  constructor(
    private store: Store
  ) {
  }

  ngOnInit(): void {
    this.isLoading$ = this.store.pipe(select(selectIsLoading), shareReplay(1));
  }

  loadMovies() {
    this.store.dispatch(loadMovies());
  }
}
