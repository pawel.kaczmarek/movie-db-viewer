import {MdbvHeaderComponent} from '@app/modules/header/header.component';
import {MdbvMovieSearchBarComponent} from '@app/modules/search/search-bar/movie-search-bar.component';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MockComponent} from 'ng-mocks';

describe('Component: MdbvHeaderComponent', () => {
  let spectator: Spectator<MdbvHeaderComponent>;

  const createComponent = createComponentFactory({
    component: MdbvHeaderComponent,
    declarations: [
      MockComponent(MdbvMovieSearchBarComponent),
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });
});
