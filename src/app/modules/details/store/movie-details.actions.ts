import {ApiError, Movie, MovieDetails} from '@app/app.models';
import {createAction, props} from '@ngrx/store';

export const loadDetails = createAction('[Details] Load Details', props<{movie: Movie}>());
export const loadDetailsComplete = createAction('[Details] Load Details Complete', props<{movie: MovieDetails}>());
export const loadDetailsFailed = createAction('[Details] Load Details Failed', props<{error: ApiError}>());
