/* tslint:disable:no-any */
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ErrorsService} from '@app/errors.service';
import {environment} from '@env/environment';
import {ToastrService} from 'ngx-toastr';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(
    private toastr: ToastrService,
    private errorService: ErrorsService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(response => {
        this.toastr.error(this.getErrorMessage(response.error), 'API error');
        this.logError(response);
        return of(response);
      }),
    );
  }

  private getErrorMessage(error: any) {
    let message = 'API Error occurred';
    if (typeof error === 'string') {
      message = error;
    }
    if (error?.status_message) {
      message = error.status_message;
    }
    return message;
  }

  private logError(error: any) {
    if (environment.production) {
      this.errorService.log(error);
    }
  }
}
