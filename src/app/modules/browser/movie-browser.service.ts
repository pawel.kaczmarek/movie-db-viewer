import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Movie, Pageable} from '@app/app.models';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieBrowserService {
  constructor(
    private http: HttpClient
  ) { }

  getPopular(page: number = 1): Observable<Pageable<Movie>> {
    const url = `${environment.movieDb.apiUrl}/discover/movie`;
    const params = new HttpParams()
      .append('sort_by', 'popularity.desc')
      .append('page', page.toString());
    return this.http.get<Pageable<Movie>>(url, { params });
  }
}
