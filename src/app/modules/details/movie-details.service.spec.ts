import {MovieDetails} from '@app/app.models';
import {environment} from '@env/environment';
import {createHttpFactory, HttpMethod, SpectatorHttp} from '@ngneat/spectator';
import {MovieDetailsService} from './movie-details.service';

describe('Service: MovieDetailsService', () => {
  let spectator: SpectatorHttp<MovieDetailsService>;
  const createHttp = createHttpFactory(MovieDetailsService);
  const API_URL = 'some.url';

  beforeEach(() => {
    spectator = createHttp();
    environment.movieDb.apiUrl = API_URL;
  });

  it('should fetch movie details', () => {
    const response = {} as MovieDetails;
    spectator.service.getDetails(1).subscribe(res => {
      expect(res).toEqual(response);
    });
    const req = spectator.expectOne(`${API_URL}/movie/1`, HttpMethod.GET);
    req.flush(response);
  });
});
