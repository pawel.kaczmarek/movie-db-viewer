import {ApiError, Movie, Pageable} from '@app/app.models';
import {createAction, props} from '@ngrx/store';

export const selectMovie = createAction('[Browser] Select Movie', props<{id: number}>());
export const deselectMovie = createAction('[Browser] Deselect Movie');

export const clearMovies = createAction('[Browser] Clear Movies');
export const loadMovies = createAction('[Browser] Load Movies');
export const loadMoviesComplete = createAction('[Browser] Load Movies Complete', props<{page: Pageable<Movie>}>());
export const loadMoviesFailed = createAction('[Browser] Load Movies Failed', props<{error: ApiError}>());
