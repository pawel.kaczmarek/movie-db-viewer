import {loadMovies, selectIsLoading} from '@app/modules/browser/store';
import {MdbvLoadingIconComponent} from '@app/modules/illustrations/loading-icon/loading-icon.component';
import {AppState} from '@app/store/app.store';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MemoizedSelector} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {MockComponent} from 'ng-mocks';
import {MdbvLoadMoreButtonComponent} from './load-more-button.component';

describe('Component: MdbvLoadMoreButtonComponent', () => {
  let spectator: Spectator<MdbvLoadMoreButtonComponent>;
  let store: MockStore;
  let isLoadingSelector: MemoizedSelector<AppState, boolean>;

  const getButton = () => spectator.query('button');
  const getLoadingIcon = () => spectator.query(MdbvLoadingIconComponent);

  const createComponent = createComponentFactory({
    component: MdbvLoadMoreButtonComponent,
    declarations: [
      MockComponent(MdbvLoadingIconComponent),
    ],
    detectChanges: false,
    providers: [
      provideMockStore({}),
    ]
  });

  beforeEach(() => {
    spectator = createComponent();
    store = spectator.inject(MockStore);
    spyOn(store, 'dispatch');
    isLoadingSelector = store.overrideSelector(selectIsLoading, false);
    spectator.detectChanges();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });

  describe('when is not loading, button', () => {
    beforeEach(() => {
      isLoadingSelector.setResult(false);
      store.refreshState();
      spectator.detectChanges();
    });
    it('should be enabled', () => {
      expect(getButton()).not.toBeDisabled();
    });
    it('should show "Load More" text', () => {
      expect(getButton()).toContainText('Load More');
    });
    it('should dispatch load movies action when clicked', () => {
      spectator.click(getButton());
      spectator.detectChanges();
      expect(store.dispatch).toHaveBeenCalledWith(loadMovies());
    });
  });

  describe('when is loading, button', () => {
    beforeEach(() => {
      isLoadingSelector.setResult(true);
      store.refreshState();
      spectator.detectChanges();
    });
    it('should be disabled', () => {
      expect(getButton()).toBeDisabled();
    });
    it('should show loading icon', () => {
      expect(getLoadingIcon()).toExist();
    });
  });
});
