import {Directive, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {environment} from '@env/environment';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, filter} from 'rxjs/internal/operators';

@Directive({
  selector: 'input[mdbvInputDebounce]'
})
export class MdbvInputDebounceDirective implements OnInit, OnDestroy {

  private valueChange$ = new Subject<string>();
  private subscription = new Subscription();

  @HostListener('keyup', ['$event']) keyUp($event) {
    this.valueChange$.next($event?.target?.value);
  }

  @Output() readonly mdbvInputDebounce = new EventEmitter<string>();
  @Input() allowEmpty = false;

  ngOnInit(): void {
    this.subscription.add(this.valueChange$.pipe(
      debounceTime(environment.debounce),
      filter(query => this.allowEmpty || Boolean(query)),
    ).subscribe(
  query => this.mdbvInputDebounce.emit(query)
    ));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
