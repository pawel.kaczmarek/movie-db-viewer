import {Movie, Pageable} from '@app/app.models';
import {pickSearchResult, searchMovie, searchMovieComplete, searchMovieFailed} from '@app/modules/search/store/movie-search.actions';
import {searchReducer, SearchState} from '@app/modules/search/store/movie-search.reducer';

describe('Store: Movie Search Reducer', () => {
  let state: SearchState;
  let afterState: SearchState;
  const movies = [{id: 1}, {id: 2}, {id: 3}];
  const picks = [{id: 40}, {id: 40}, {id: 40}, {id: 1}, {id: 1}, {id: 50}];
  beforeEach(() => {
    state = {
      entities: {1: movies[0], 2: movies[1], 3: movies[2]},
      ids: [1, 2, 3],
      isLoading: false,
      userPicks: picks,
    };
  });

  it('should handle searchMovie', () => {
    afterState = searchReducer(state, searchMovie({query: 'snafu'}));
    expect(afterState).toEqual({
      ...state,
      entities: {},
      ids: [],
      isLoading: true,
    });
  });

  it('should handle searchMovieComplete', () => {
    afterState = searchReducer(state, searchMovieComplete({
      page: {
        results: [{id: 10}, {id: 20}],
        page: 6,
        total_pages: 7
      } as Pageable<Movie>
    }));
    expect(afterState).toEqual({
      ...state,
      entities: {10: {id: 10}, 20: {id: 20}},
      ids: [10, 20],
      isLoading: false
    });
  });

  it('should handle searchMovieFailed', () => {
    afterState = searchReducer(state, searchMovieFailed({error: {}}));
    expect(afterState).toEqual({
      ...state,
      isLoading: false,
    });
  });

  it('should handle pickSearchResult', () => {
    afterState = searchReducer(state, pickSearchResult({id: 3}));
    expect(afterState).toEqual({
      ...state,
      userPicks: [{id: 3}, {id: 40}, {id: 40}, {id: 40}, {id: 1}, {id: 1}, {id: 50}]
    });
  });
});
