import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {GlobalErrorHandler} from '@app/global-error-handler';
import {HttpErrorInterceptor} from '@app/interceptors/http-error.interceptor';
import {MdbvMovieDetailsModule} from '@app/modules/details/movie-details.module';
import {MdbvHeaderModule} from '@app/modules/header/header.module';
import {environment} from '@env/environment';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {NglToastModule} from 'ng-lightning';
import {ToastrModule} from 'ngx-toastr';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AttachApiKeyInterceptor} from './interceptors/attach-api-key.interceptor';
import {MdbvMovieBrowserModule} from './modules/browser/movie-browser.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    MdbvHeaderModule,
    MdbvMovieBrowserModule,
    MdbvMovieDetailsModule,
    ToastrModule.forRoot(),
    EffectsModule.forRoot(),
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    NglToastModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AttachApiKeyInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
