import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Movie, Pageable} from '@app/app.models';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieSearchService {
  constructor(
    private http: HttpClient
  ) { }

  search(query: string): Observable<Pageable<Movie>> {
    const url = `${environment.movieDb.apiUrl}/search/movie`;
    const params = new HttpParams()
      .append('query', query);
    return this.http.get<Pageable<Movie>>(url, { params });
  }
}
