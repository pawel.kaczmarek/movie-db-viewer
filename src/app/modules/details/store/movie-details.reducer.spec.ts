import {Movie, MovieDetails} from '@app/app.models';
import {loadDetails, loadDetailsComplete, loadDetailsFailed} from '@app/modules/details/store/movie-details.actions';
import {movieDetailsReducer, MovieDetailsState} from '@app/modules/details/store/movie-details.reducer';

describe('Store: Movie Details Reducer', () => {
  let state: MovieDetailsState;
  let afterState: MovieDetailsState;
  beforeEach(() => {
    state = {
      movie: null,
      isLoading: false,
      loadedMovieId: 10,
    };
  });

  it('should handle loadDetails', () => {
    const movie: Movie = {id: 1};
    afterState = movieDetailsReducer(state, loadDetails({movie}));
    expect(afterState).toEqual({
      isLoading: true,
      movie,
      loadedMovieId: 10
    });
  });

  it('should handle loadDetailsComplete', () => {
    const movieDetails: MovieDetails = {id: 20};
    afterState = movieDetailsReducer(state, loadDetailsComplete({movie: movieDetails}));
    expect(afterState).toEqual({
      isLoading: false,
      movie: movieDetails,
      loadedMovieId: 20
    });
  });

  it('should handle loadDetailsFailed', () => {
    afterState = movieDetailsReducer(state, loadDetailsFailed({error: {}}));
    expect(afterState).toEqual({
      isLoading: false,
      movie: null,
      loadedMovieId: 10
    });
  });
});
