import {ChangeDetectionStrategy, Component, HostListener, Input} from '@angular/core';
import {Movie} from '@app/app.models';
import {loadDetails} from '@app/modules/details/store';
import {Store} from '@ngrx/store';
import {selectMovie} from '../../store';

@Component({
  selector: 'mdbv-movie-list-item',
  templateUrl: './movie-list-item.component.html',
  styleUrls: ['./movie-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdbvMovieListItemComponent {

  @Input() movie: Movie;

  @HostListener('click')
  selectMovie() {
    this.store.dispatch(selectMovie({id: this.movie.id}));
    this.store.dispatch(loadDetails({movie: this.movie}));
  }

  constructor(
    private store: Store
  ) {
  }
}
