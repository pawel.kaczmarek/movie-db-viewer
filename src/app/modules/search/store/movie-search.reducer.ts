import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';
import {Movie} from '../../../app.models';
import {
  pickSearchResult,
  searchMovie,
  searchMovieComplete,
  searchMovieFailed,
} from './movie-search.actions';

export const searchStateKey = 'search';

export interface SearchState extends EntityState<Movie> {
  isLoading: boolean;
  userPicks: Movie[];
}

export const initialState: SearchState = {
  entities: {},
  ids: [],
  isLoading: false,
  userPicks: [],
};

export const searchReducer = createReducer(
  initialState,
  on(searchMovie, state => adapter.removeAll({
    ...state,
    isLoading: true
  })),
  on(searchMovieComplete, (state, {page}) => adapter.upsertMany(
    page.results, adapter.removeAll(state)
  )),
  on(searchMovieComplete, searchMovieFailed, state => ({...state, isLoading: false})),
  on(pickSearchResult, (state, {id}) => ({
    ...state,
    userPicks: [
      state.entities[id],
      ...state.userPicks,
    ]
  }))
);

export const adapter: EntityAdapter<Movie> = createEntityAdapter<Movie>();
