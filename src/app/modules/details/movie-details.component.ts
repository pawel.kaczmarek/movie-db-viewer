import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {MovieDetails} from '@app/app.models';
import {selectMovie} from '@app/modules/details/store';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

@Component({
  selector: 'mdbv-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdbvMovieDetailsComponent implements OnInit {

  details$: Observable<MovieDetails>;

  constructor(
    private store: Store,
  ) {
  }

  ngOnInit(): void {
    this.details$ = this.store.pipe(select(selectMovie));
  }
}
