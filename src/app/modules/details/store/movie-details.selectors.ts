import {AppState} from '@app/store/app.store';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {detailsStateKey, MovieDetailsState} from './movie-details.reducer';

export const selectMovieDetails = createFeatureSelector<AppState, MovieDetailsState>(detailsStateKey);

export const loadedMovieId = createSelector(selectMovieDetails, state => state.loadedMovieId);
export const selectIsLoading = createSelector(selectMovieDetails, state => state.isLoading);
export const selectMovie = createSelector(selectMovieDetails, state => state.movie);
