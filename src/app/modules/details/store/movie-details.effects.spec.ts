import {createServiceFactory, SpectatorService} from '@ngneat/spectator';
import {provideMockActions} from '@ngrx/effects/testing';
import {Action} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {MovieDetailsService} from '../movie-details.service';
import {loadDetails} from './movie-details.actions';
import {MovieDetailsEffects} from './movie-details.effects';
import SpyObj = jasmine.SpyObj;

xdescribe('Store: Movie Details Effects', () => {
  let spectator: SpectatorService<MovieDetailsEffects>;
  let detailsService: SpyObj<MovieDetailsService>;
  let actions$ = new Observable<Action>();

  const createService = createServiceFactory({
    service: MovieDetailsEffects,
    mocks: [
      MovieDetailsService,
    ],
    providers: [
      provideMockActions(() => actions$),
    ]
  });

  beforeEach(() => {
    spectator = createService();
    detailsService = spectator.inject(MovieDetailsService);
  });

  it('should create', () => {
    expect(spectator.service).toBeDefined();
  });

  describe('load details effect', () => {
    let movie;
    beforeEach(() => {
      movie = {id: 1};
      actions$ = of(loadDetails({movie}));
    });
    it('should load movie details', () => {
      spectator.service.loadDetails$.subscribe();
      expect(detailsService.getDetails).toHaveBeenCalledWith(movie.id);
    });
  });
});
