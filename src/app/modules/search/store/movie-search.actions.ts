import {createAction, props} from '@ngrx/store';
import {ApiError, Movie, Pageable} from '../../../app.models';

export const searchMovie = createAction('[Search] Search Movie', props<{query: string}>());
export const searchMovieComplete = createAction('[Search] Search Movie Complete', props<{page: Pageable<Movie>}>());
export const searchMovieFailed = createAction('[Search] Search Movie Failed', props<{error: ApiError}>());

export const pickSearchResult = createAction('[Search] Pick Search Result', props<{id: number}>());
export const pickPreviousPick = createAction('[Search] Pick Previous Pick', props<{id: number}>());
