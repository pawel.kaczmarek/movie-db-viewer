import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {of} from 'rxjs';
import {catchError, concatMap, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {MovieBrowserService} from '../movie-browser.service';
import {loadMovies, loadMoviesComplete, loadMoviesFailed} from './browser-movies.actions';
import {selectCurrentPage} from './browser-movies.selectors';

@Injectable()
export class BrowserMoviesEffects {

  constructor(
    private store: Store,
    private actions$: Actions,
    private browseService: MovieBrowserService
  ) {}

  loadMovies$ = createEffect(() => this.actions$.pipe(
    ofType(loadMovies),
    concatMap(action => of(action).pipe(
      withLatestFrom(this.store.pipe(select(selectCurrentPage)))
    )),
    switchMap(([action, pageNumber]) => this.browseService.getPopular(pageNumber + 1).pipe(
      map(page => loadMoviesComplete({page}),
      catchError( error => of(loadMoviesFailed({error})))
    )))
  ));
}
