import {Movie} from '@app/app.models';
import {MdbvMovieListItemComponent} from '@app/modules/browser/list/item/movie-list-item.component';
import {selectMovie} from '@app/modules/browser/store';
import {loadDetails} from '@app/modules/details/store';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MockStore, provideMockStore} from '@ngrx/store/testing';

describe('Component: MdbvMovieListItemComponent', () => {
  let spectator: Spectator<MdbvMovieListItemComponent>;
  let store: MockStore;
  const movie: Movie = {title: 'Docile Max II', id: 1};

  const createComponent = createComponentFactory({
    component: MdbvMovieListItemComponent,
    providers: [
      provideMockStore({}),
    ]
  });

  beforeEach(() => {
    spectator = createComponent({
      props: {movie}
    });
    store = spectator.inject(MockStore);
    spyOn(store, 'dispatch');
    spectator.detectChanges();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });

  it('should display movie title', () => {
    expect(spectator.fixture.nativeElement).toContainText(movie.title);
  });

  describe('when clicked', () => {
    beforeEach(() => {
      spectator.click(spectator.fixture.nativeElement);
      spectator.detectChanges();
    });
    it('should dispatch select movie action when clicked', () => {
      expect(store.dispatch).toHaveBeenCalledWith(selectMovie({id: movie.id}));
    });
    it('should dispatch load details action', () => {
      expect(store.dispatch).toHaveBeenCalledWith(loadDetails({movie}));
    });
  });
});
