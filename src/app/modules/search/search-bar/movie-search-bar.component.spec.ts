import {Movie} from '@app/app.models';
import {selectMovie} from '@app/modules/browser/store';
import {loadDetails} from '@app/modules/details/store';
import {MdbvInputDebounceDirective} from '@app/modules/input-debounce/input-debounce.directive';
import {MdbvMovieSearchBarComponent} from '@app/modules/search/search-bar/movie-search-bar.component';
import {pickPreviousPick, pickSearchResult, searchMovie} from '@app/modules/search/store';
import {AppState} from '@app/store/app.store';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MemoizedSelectorWithProps} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {testSubjSelector} from '@testing';
import {MockDirective} from 'ng-mocks';
import {selectLastPicks, selectTopSearchResults} from '../store/movie-search.selectors';

describe('Component: MdbvMovieSearchBarComponent', () => {
  let spectator: Spectator<MdbvMovieSearchBarComponent>;
  let store: MockStore;
  let lastPicksSelector: MemoizedSelectorWithProps<AppState, { limit: number }, Movie[]>;
  let searchResultsSelector: MemoizedSelectorWithProps<AppState, { limit: number }, Movie[]>;
  let testConfig: { searchItems: Movie[]; userPicks: Movie[] };

  const getSearchItems = () => spectator.queryAll(testSubjSelector('mdbv-movie-search-bar-search-result-item'));
  const getUserPickItems = () => spectator.queryAll(testSubjSelector('mdbv-movie-search-bar-user-pick-item'));

  const createComponent = createComponentFactory({
    component: MdbvMovieSearchBarComponent,
    declarations: [
      MockDirective(MdbvInputDebounceDirective),
    ],
    detectChanges: false,
    providers: [
      provideMockStore({}),
    ]
  });

  beforeEach(() => {
    testConfig = {
      searchItems: [
        {title: 'one', id: 1},
        {title: 'two', id: 2},
        {title: 'three', id: 3},
      ],
      userPicks: [
        {title: 'pone', id: 10},
        {title: 'ptwo', id: 20},
        {title: 'pthree', id: 30},
      ]
    };
  });

  beforeEach(() => {
    spectator = createComponent();
    store = spectator.inject(MockStore);
    spyOn(store, 'dispatch');
    lastPicksSelector = store.overrideSelector(selectLastPicks, testConfig.userPicks);
    searchResultsSelector = store.overrideSelector(selectTopSearchResults, testConfig.searchItems);
    spectator.detectChanges();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });

  it('should render search items', () => {
    const items = getSearchItems();
    expect(items.length).toEqual(3);
    expect(items[0]).toContainText(testConfig.searchItems[0].title);
    expect(items[1]).toContainText(testConfig.searchItems[1].title);
    expect(items[2]).toContainText(testConfig.searchItems[2].title);
  });

  it('should render user last picks', () => {
    const pick = getUserPickItems();
    expect(pick.length).toEqual(3);
    expect(pick[0]).toContainText(testConfig.userPicks[0].title);
    expect(pick[1]).toContainText(testConfig.userPicks[1].title);
    expect(pick[2]).toContainText(testConfig.userPicks[2].title);
  });

  it('should dispatch search event when search input directive emits query', () => {
    const query = 'search this!';
    spectator.triggerEventHandler(
      MdbvInputDebounceDirective,
      'mdbvInputDebounce',
      query
    );
    spectator.detectChanges();
    expect(store.dispatch).toHaveBeenCalledWith(searchMovie({query}));
  });

  describe('when search result is clicked', () => {
    beforeEach(() => {
      spectator.click(getSearchItems()[1]);
      spectator.detectChanges();
    });
    it('should dispatch pick search result action', () => {
      expect(store.dispatch).toHaveBeenCalledWith(pickSearchResult({id: 2}));
    });
    it('should dispatch select movie action', () => {
      expect(store.dispatch).toHaveBeenCalledWith(selectMovie({id: 2}));
    });
    it('should dispatch load details action', () => {
      expect(store.dispatch).toHaveBeenCalledWith(loadDetails({movie: testConfig.searchItems[1]}));
    });
  });

  describe('when last user pick is clicked', () => {
    beforeEach(() => {
      spectator.click(getUserPickItems()[1]);
      spectator.detectChanges();
    });
    it('should dispatch pick search result action', () => {
      expect(store.dispatch).toHaveBeenCalledWith(pickPreviousPick({id: 20}));
    });
    it('should dispatch select movie action', () => {
      expect(store.dispatch).toHaveBeenCalledWith(selectMovie({id: 20}));
    });
    it('should dispatch load details action', () => {
      expect(store.dispatch).toHaveBeenCalledWith(loadDetails({movie: testConfig.userPicks[1]}));
    });
  });
});
