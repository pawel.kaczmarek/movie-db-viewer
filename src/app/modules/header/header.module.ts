import {NgModule} from '@angular/core';
import {MdbvMovieSearchModule} from '@app/modules/search/movie-search.module';
import {MdbvHeaderComponent} from './header.component';

@NgModule({
  declarations: [
    MdbvHeaderComponent,
  ],
  imports: [
    MdbvMovieSearchModule,
  ],
  exports: [
    MdbvHeaderComponent,
  ]
})
export class MdbvHeaderModule { }
