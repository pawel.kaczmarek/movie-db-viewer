import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Movie} from '@app/app.models';
import {selectMovie} from '@app/modules/browser/store';
import {loadDetails} from '@app/modules/details/store';
import {pickPreviousPick, pickSearchResult, searchMovie, selectLastPicks, selectTopSearchResults} from '@app/modules/search/store';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {shareReplay} from 'rxjs/internal/operators';

@Component({
  selector: 'mdbv-movie-search-bar',
  templateUrl: './movie-search-bar.component.html',
  styleUrls: ['./movie-search-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdbvMovieSearchBarComponent {

  userPicks$: Observable<Movie[]>;
  searchResults$: Observable<Movie[]>;

  constructor(
    private store: Store,
  ) {
    this.searchResults$ = this.store.pipe(
      select(selectTopSearchResults, {limit: 5}),
      shareReplay(1)
    );
    this.userPicks$ = this.store.pipe(
      select(selectLastPicks, {limit: 4}),
      shareReplay(1),
    );
  }

  pickResult(movie: Movie) {
    this.store.dispatch(pickSearchResult({id: movie.id}));
    this.store.dispatch(selectMovie({id: movie.id}));
    this.store.dispatch(loadDetails({movie}));
  }

  pickPreviousPick(movie: Movie) {
    this.store.dispatch(pickPreviousPick({id: movie.id}));
    this.store.dispatch(selectMovie({id: movie.id}));
    this.store.dispatch(loadDetails({movie}));
  }

  search(query: string) {
    this.store.dispatch(searchMovie({query}));
  }
}
