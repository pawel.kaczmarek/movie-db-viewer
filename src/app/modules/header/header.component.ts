import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'mdbv-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MdbvHeaderComponent implements OnInit {

  ngOnInit(): void {
  }

}
