import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Movie} from '@app/app.models';
import {loadDetails} from '@app/modules/details/store';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {shareReplay} from 'rxjs/internal/operators';
import {clearMovies, loadMovies, selectAllMovies, selectIsLastPage, selectMovie, selectSelectedId} from '../store';

@Component({
  selector: 'mdbv-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdbvMovieListComponent implements OnInit {

  movies$: Observable<Movie[]>;
  selectedId$: Observable<number>;
  isLastPage$: Observable<boolean>;

  constructor(
    private store: Store
  ) {
  }

  ngOnInit(): void {
    this.clearList();
    this.loadMovies();
    this.selectedId$ = this.store.pipe(select(selectSelectedId));
    this.movies$ = this.store.pipe(select(selectAllMovies), shareReplay(1));
    this.isLastPage$ = this.store.pipe(select(selectIsLastPage));
  }

  trackById(index: number, movie: Movie): number {
    return movie.id;
  }

  clearList() {
    this.store.dispatch(clearMovies());
  }

  selectMovie(movie: Movie) {
    this.store.dispatch(selectMovie({id: movie.id}));
    this.store.dispatch(loadDetails({movie}));
  }

  loadMovies() {
    this.store.dispatch(loadMovies());
  }
}
