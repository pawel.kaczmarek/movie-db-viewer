# Movie Db Viewer

Quick portfolio project done as task in a recrutation process - main principle is to showcase approach to store management and structuring the code. 

Utilizes [The Movie Database API](https://www.themoviedb.org/).

Shows list of popular movies and provides user with simple search mechanism.

Developing it was an occasion to get to know and play around a bit with lightning design system.

[Working demo can be found here](https://movie-db-viewer.web.app/)

## Tech stack

* Angular 10
* NgRX 10
* [Lightning Design System](https://lightningdesignsystem.com/) with limited usage of its angular wrapper [ng-lightning](https://ng-lightning.github.io/ng-lightning) 
* Karma / Jasmine / [Spectator](https://github.com/ngneat/spectator) / [ng-mocks](https://github.com/ike18t/ng-mocks)

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. 

Run `npm run build:prod` for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
