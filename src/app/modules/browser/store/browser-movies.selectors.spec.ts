import {BrowserState} from '@app/modules/browser/store/browser-movies.reducer';
import {selectAllMovies, selectCurrentPage, selectIsLastPage, selectIsLoading, selectSelectedId} from '@app/modules/browser/store/browser-movies.selectors';

describe('Store: Movie Browser Selectors', () => {
  let state: BrowserState;
  const movies = [{id: 1}, {id: 2}, {id: 3}];
  beforeEach(() => {
    state = {
      entities: {1: movies[0], 2: movies[1], 3: movies[2]},
      ids: [1, 2, 3],
      currentPage: 1,
      isLastPage: true,
      selectedItemId: 10,
      isLoading: true,
    };
  });

  it('should return selected movie id', () => {
    expect(selectSelectedId.projector(state)).toBe(state.selectedItemId);
  });

  it('should return current page', () => {
    expect(selectCurrentPage.projector(state)).toBe(state.currentPage);
  });

  it('should return is last page', () => {
    expect(selectIsLastPage.projector(state)).toBe(state.isLastPage);
  });

  it('should return is loading', () => {
    expect(selectIsLoading.projector(state)).toBe(state.isLoading);
  });

  it('should return all movies', () => {
    expect(selectAllMovies.projector(state)).toEqual(movies);
  });
});
