import {MovieDetails} from '@app/app.models';
import {MdbvMovieDetailsComponent} from '@app/modules/details/movie-details.component';
import {MdbvMoviePosterComponent} from '@app/modules/details/poster/movie-poster.component';
import {selectMovie} from '@app/modules/details/store';
import {MdbvNoContentIllustrationComponent} from '@app/modules/illustrations/no-content/no-content-illustration.component';
import {AppState} from '@app/store/app.store';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {MemoizedSelector} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {testSubjSelector} from '@testing';
import {NglRating} from 'ng-lightning';
import {MockComponent} from 'ng-mocks';

describe('Component: MdbvMovieDetailsComponent', () => {
  let spectator: Spectator<MdbvMovieDetailsComponent>;
  let store: MockStore;
  let detailsSelector: MemoizedSelector<AppState, MovieDetails>;

  const getNoContentPlaceholder = () => spectator.query(MdbvNoContentIllustrationComponent);
  const getMovieDetailsCard = () => spectator.query(testSubjSelector('mdbv-movie-details-card'));
  const getHeader = () => spectator.query(testSubjSelector('mdbv-movie-details-header'));
  const getRating = () => spectator.query(NglRating);
  const getPoster = () => spectator.query(MdbvMoviePosterComponent);
  const getOverview = () => spectator.query(testSubjSelector('mdbv-movie-details-overview'));

  const createComponent = createComponentFactory({
    component: MdbvMovieDetailsComponent,
    declarations: [
      MockComponent(NglRating),
      MockComponent(MdbvMoviePosterComponent),
      MockComponent(MdbvNoContentIllustrationComponent),
    ],
    detectChanges: false,
    providers: [
      provideMockStore({}),
    ]
  });

  beforeEach(() => {
    spectator = createComponent();
    store = spectator.inject(MockStore);
    detailsSelector = store.overrideSelector(selectMovie, null);
    spectator.detectChanges();
  });

  it('should create', () => {
    expect(spectator.component).toExist();
  });

  describe('when no details are available', () => {
    beforeEach(() => {
      detailsSelector.setResult(null);
      store.refreshState();
      spectator.detectComponentChanges();
    });
    it('should display no details placeholder', () => {
      expect(getNoContentPlaceholder()).toExist();
    });
    it('should not display details card', () => {
      expect(getMovieDetailsCard()).not.toExist();
    });
  });

  describe('when details are available', () => {
    let details: MovieDetails;
    beforeEach(() => {
      details = {
        title: 'foobar in hell',
        vote_average: 7,
        poster_path: 'path',
        overview: 'Hell turns even hotter'
      };
      detailsSelector.setResult(details);
      store.refreshState();
      spectator.detectChanges();
    });
    it('should show header', () => {
      expect(getHeader()).toContainText(details.title);
    });
    it('should show rating', () => {
      expect(getRating().rate).toEqual(details.vote_average / 2);
    });
    it('should show poster', () => {
      expect(getPoster().url).toEqual(details.poster_path);
    });
    it('should show overview', () => {
      expect(getOverview()).toContainText(details.overview);
    });
  });
});
