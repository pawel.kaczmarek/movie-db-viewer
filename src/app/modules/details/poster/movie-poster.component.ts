import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {environment} from '@env/environment';

const NO_POSTER_IMAGE = '/assets/no-movie-icon.svg';

@Component({
  selector: 'mdbv-movie-poster',
  templateUrl: './movie-poster.component.html',
  styleUrls: ['movie-poster.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MdbvMoviePosterComponent {

  isLoaded = false;
  posterUrl: string | null;

  @Input() set url(value: string | null) {
    this.posterUrl = value;
    this.isLoaded = false;
  }

  get url(): string {
    return this.posterUrl
      ? `${environment.movieDb.imageUrl}${this.posterUrl}`
      : NO_POSTER_IMAGE;
  }

  imageLoaded() {
    this.isLoaded = true;
  }
}
