import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MovieDetails} from '@app/app.models';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsService {
  constructor(
    private http: HttpClient
  ) { }

  getDetails(id: number): Observable<MovieDetails> {
    const url = `${environment.movieDb.apiUrl}/movie/${id}`;
    return this.http.get<MovieDetails>(url);
  }
}
